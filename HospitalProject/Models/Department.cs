﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Department
    {
        [Key]
        public int DepartmentID { get; set; }

        [Required, StringLength(200), Display(Name = "Department Name")]
        public string DepartmentName { get; set; }
    }
}
