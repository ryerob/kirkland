﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Donation
    {
        [Key]
        public int DonationID { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string DonationFName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string DonationLName { get; set; }

        [Required, StringLength(355), Display(Name = "Address")]
        public string DonationAddress { get; set; }

        [Required, StringLength(100), Display(Name = "City")]
        public string DonationCity { get; set; }

        [Required, StringLength(100), Display(Name = "Country")]
        public string DonationCountry { get; set; }

        [Required, StringLength(255), Display(Name = "Postal Code")]
        public string DonationPostalCode { get; set; }

        [Required, StringLength(255), Display(Name = "Email")]
        public string DonationEmail { get; set; }

        // Need to set default date in the DBContext when we make it --> https://github.com/aspnet/EntityFrameworkCore/issues/10601
        [Required, StringLength(255)]
        public DateTime DonationDate { get; set; }

        [Required, Display(Name = "Donation Amount")]
        public int DonationAmount { get; set; }

        [StringLength(250), Display(Name = "Comments")]
        public string DonationComments { get; set; }

        //Will be true or false 1=> Yes 2=> No (2 is default)
        [Display(Name = "Remain Anonymous")]
        public int DonationAnon { get; set; }

    }
}
