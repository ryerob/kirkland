﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Admin
    {
        [Key]
        public int AdminID { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string AdminFName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string AdminLName { get; set; }

        [Required, StringLength (255), Display(Name = "Username")]
        public string AdminUsernmae { get; set; }

        [StringLength(255), Display(Name ="Staff Role:")]
        public string AdminRole { get; set; }
    }
}
