﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class ECard
    {
        [Key]
        public int ECardID { get; set; }

        [ForeignKey("ECardMessage")]
        public int ECardMessageID { get; set; }

        [ForeignKey("ECardTemplate")]
        public int ECardTemplateID { get; set; }

        [StringLength(250), Display(Name ="Custom Message")]
        public string ECardCustomMessage { get; set; }

        [Required, StringLength(225), Display(Name ="User Email")]
        public string ECardUserEmail { get; set; }

        [Required, StringLength(225), Display(Name = "Patient Email")]
        public string ECardPatientEmail { get; set; }

        //Make sure to set the default datetime in the DBContext when set up
        [Required]
        public DateTime ECardDate { get; set; }


        public virtual ECardMessage ECardMessage { get; set; }
        public virtual ECardTemplate ECardTemplate { get; set; }


    }
}
