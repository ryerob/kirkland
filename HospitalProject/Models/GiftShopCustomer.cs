﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class GiftShopCustomer
    {
        [Key]
        public int CustomerID { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string CustomerFirstName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string CustomerLastName { get; set; }

        [Required, StringLength(255), Display(Name = "Email")]
        public string CustomerEmail { get; set; }

        [Required, StringLength(255), Display(Name = "Payment Method")]
        public string CustomerPaymentMethod { get; set; }

        [ForeignKey("GiftShopProduct")]
        public int ProductID { get; set; }

        [ForeignKey("Department")]
        public int DepartmentID { get; set; }

        [Required]
        public DateTime PurchaseDate { get; set; }

        public virtual Department Department { get; set; }
        public virtual GiftShopProduct GiftShopProduct { get; set; }  //a customer can buy just one item at the time, there is no a shopping cart
   
    }
}
