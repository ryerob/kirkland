﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Job
    {
        [Key]
        public int JobID { get; set; }

        [Required, StringLength(225), Display(Name = "Title")]
        public string JobTitle { get; set; }

        [Required, StringLength(500), Display(Name = "Description")]
        public string JobDescription { get; set; }

        [Required, Display(Name = "Posted On")]
        public DateTime JobPostDate { get; set; }

        [Required, Display (Name = "Closing Date")]
        public DateTime JobClosingDate { get; set; }

        [Required, StringLength(500), Display(Name = "Requirements")]
        public string JobRequirements { get; set; }

        [Required, StringLength(200), Display(Name = "Contact Email")]
        public string JobContactEmail { get; set; }

        [ForeignKey ("DepartmentID")]
        public int DepartmentID { get; set; }

        public virtual Department Department { get; set; }

        [Required, StringLength(500), Display(Name = "Application Requirements")]
        public string JobApplicationRequirements { get; set; }

    }
}
