﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Feedback
    {
        [Key]
        public int FeedbackID { get; set; }

        [ForeignKey("Department")]
        public int DepartmentID { get; set; }

        [StringLength(255), Display(Name = "Name")]
        public string FeedbackUserName { get; set; }

        [Required, StringLength(255), Display(Name = "Email")]
        public string FeedbackUserEmail { get; set; }

        [Required, StringLength(225), Display(Name = "Message Type")]
        public string FeedbackMessageType { get; set; }

        [Required, StringLength(225), Display(Name = "Message Subject")]
        public string FeedbackMessageSubject { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Message Content")]
        public string FeedbackMessageContent { get; set; }

        [Required]
        public DateTime FeedbackPosted { get; set; }

        public virtual Department Department { get; set; }

    }
}
