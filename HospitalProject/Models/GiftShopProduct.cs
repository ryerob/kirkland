﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class GiftShopProduct
    {
        [Key]
        public int ProductID { get; set; }

        [Required, StringLength(255), Display(Name = "Product Name")]
        public string ProductName { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Product Description")]
        public string ProductDescription { get; set; }

        [Required, Display(Name = "Product Price")]
        public int ProductPrice { get; set; }

        [Required, Display(Name = "Product Quantity")]
        public int ProductQuantity { get; set; }

        [Required, Display(Name = "Product Quantity Sold")]
        public int ProductQuantitySold { get; set; }

        [Required, Display(Name = "Product Quantity Left")]
        public int ProductQuantityLeft { get; set; }
    }
}
