﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class ApplicationUser //: IndentityUser
    {
        [ForeignKey("AdminID")]
        public int? AdminID { get; set; }

        //An Application User is tied to an Admin.
        public virtual Admin admin { get; set; }
    }
}
